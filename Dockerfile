# Source: https://github.com/Andre601/blog/blob/master/requirements.txt

FROM squidfunk/mkdocs-material@sha256:2671e1d9c6f9fb4147b92ff7b1e0f1a39f84aa239554eac60d7e06cc56fbccc2
RUN apk add git
RUN pip install --no-cache-dir mkdocs-awesome-pages-plugin==2.6.1
RUN pip install --no-cache-dir mkdocs-blogging-plugin
RUN pip install --no-cache-dir mkdocs-redirects
RUN pip install --no-cache-dir Pygments
